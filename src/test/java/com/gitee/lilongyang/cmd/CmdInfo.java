package com.gitee.lilongyang.cmd;

import java.io.Serializable;
import java.nio.Buffer;

public class CmdInfo implements Serializable {
    private Buffer buffer;
    private CmdInfoEnum info;

    public Buffer getBuffer() {
        return buffer;
    }

    public CmdInfo setBuffer(Buffer buffer) {
        this.buffer = buffer;
        return this;
    }

    public CmdInfoEnum getInfo() {
        return info;
    }

    public CmdInfo setInfo(CmdInfoEnum info) {
        this.info = info;
        return this;
    }
}
