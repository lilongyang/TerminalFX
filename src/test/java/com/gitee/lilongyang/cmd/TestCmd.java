package com.gitee.lilongyang.cmd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;

public class TestCmd {
    public static void main(String[] args) throws Exception {
        CmdActuator cmd = new CmdActuator();
        while (true){
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            cmd.run(bufferedReader.readLine(),null,null);
        }
    }
}
