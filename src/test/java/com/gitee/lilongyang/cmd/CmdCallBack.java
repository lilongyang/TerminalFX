package com.gitee.lilongyang.cmd;

import java.nio.Buffer;

public interface CmdCallBack<T> {
    void call(T t);
}
