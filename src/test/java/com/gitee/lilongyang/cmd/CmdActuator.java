package com.gitee.lilongyang.cmd;

import javafx.util.Callback;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringBufferInputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class CmdActuator {
    private Queue<String> queue = new LinkedBlockingQueue<>();
    private static String ROOT = "cmd";
    private Runtime runtime = null;
    private static File HOME_DIR = new File(System.getProperty("user.home"));
    private Map<String,String > env = new HashMap<>();
    public void run(String cmd, CmdCallBack<CmdInfo> infoCallback,CmdCallBack<CmdInfo> errorCallback) throws IOException {
        runtime = Runtime.getRuntime();
        Process process = runtime.exec(new String[]{"cmd","/c",cmd});


        InputStream is = process.getInputStream();
        InputStream es = process.getErrorStream();
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is,"GBK"))) {
            String info = null;
            while ( (info = bufferedReader.readLine()) != null)
                System.out.println(info);
        }
        if (is != null){
            is.close();
        }
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(es,"GBK"))) {
            String info = null;
            while ( (info = bufferedReader.readLine()) != null)
                System.out.println(info);
        }
        if (es != null){
            es.close();
        }

        boolean isEnd = false;
        try {
            isEnd = process.waitFor(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("is end "+ isEnd);
    }

}
