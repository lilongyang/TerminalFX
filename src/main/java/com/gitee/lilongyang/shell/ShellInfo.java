package com.gitee.lilongyang.shell;

public class ShellInfo {
    private String host;
    private String username;
    private String password;
    private Integer port;

    public ShellInfo() {
    }

    public ShellInfo(String username, String password, Integer port) {
        this.username = username;
        this.password = password;
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}
